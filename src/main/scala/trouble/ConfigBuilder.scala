package trouble

import org.yaml.snakeyaml.Yaml

import java.io.{File, PrintWriter}
import scala.io.Source
import scala.util.{Failure, Success, Try}

object ConfigBuilder extends App {

  private val Family = List("mara", "tay", "tori", "tricky", "vana")
  private val Kinds = List("latex-pup", "household-pup")
  private val AphasiaPath = "aphasia"
  private val OutputPath = "built"

  private def buildAphasiaConfigs(kind: String, member: String): Unit =
    (for {
      config <- yamlConfigFor(s"$AphasiaPath/$kind/$member.yaml")
      lines = Source.fromResource(s"$AphasiaPath/base.txt").getLines().toList
    } yield lines.map(_ <-- config).mkString("\n"))
      .foreach { interpolated =>
        val baseDir = s"./$OutputPath/$AphasiaPath/$kind"
        new File(baseDir).mkdirs()
        val writer = new PrintWriter(new File(s"$baseDir/$member.txt"))
        writer.write(interpolated)
        writer.close()
      }

  private def yamlConfigFor(str: String): Option[java.util.Map[String, Any]] =
    Try(Source.fromResource(str).getLines().mkString("\n")) match {
      case Failure(_) => None
      case Success(value) => Some(new Yaml().load[java.util.Map[String, Any]](value))
    }

  for {
    member <- Family
    kind <- Kinds
  } yield buildAphasiaConfigs(kind, member)

}
