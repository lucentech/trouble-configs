import freemarker.template.{Configuration, Template, Version}

import java.io.{StringReader, StringWriter}
import scala.jdk.CollectionConverters.MapHasAsJava

package object trouble {
  implicit class StringInterpolator(s: String) {
    private val template =
      new Template("tmp", new StringReader(s), new Configuration(new Version("2.3.31")))

    def <--(data: java.util.Map[String, Any]): String = interpolatedWith(data)

    private def interpolatedWith(data: java.util.Map[String, Any]): String = {
      val out = new StringWriter()
      template.process(data, out)
      val interpolated = out.toString
      out.flush()
      interpolated
    }
  }
}
