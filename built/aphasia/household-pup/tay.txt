#
#
#           🐾 🅿 🆄 🅿 🅿 🆈 ❗
#
#           Written by Nohemi Naxxras (hairtuss)
#           Please do not resell this. It's only for personal or non-profitable use!
#           Enjoy ♥
#           PLEASE OPEN THIS IMAGE FOR MORE CLEAR EXPLANATIONS OF EACH FUNCTION:
#           https://i.imgur.com/aEDebwx.png
#
#
#
#
#
# ███████ GENERAL
NAME=???
PREFIX=ta

█ SOUNDS
00000000-0000-0000-0000-000000000000
00000000-0000-0000-0000-000000000000
00000000-0000-0000-0000-000000000000
00000000-0000-0000-0000-000000000000
00000000-0000-0000-0000-000000000000
00000000-0000-0000-0000-000000000000
b58cc151-cdb7-f73c-4d1b-09d50fffe706
90c57b29-14fb-837e-d16e-7b3cc0c20faa
643a0542-4cd7-4623-02b4-ef9daa947e79

SuccessMSG=Something feels... different! Woof! Ruff! Oh look, a squirrel!

# ███████ ❖ A U D I T O R Y   R E S T R I C T I O N S
# ██████████████████████████████████
# ███████ 🅐 DEAFEN ON COMMAND

DeafenMSG=You suddenly feel dizzy.
UnDeafenMSG=You snap back to reality.

█ DEAFEN KEYWORDS
snaps her fingers.
snaps his fingers.
snaps their fingers.
bad puppy! timeout!
shush, mutt
timeout, pet
timeout, latexpup
timeout latexpup


█ EXCEPTION WORDS
, pup?
, dog?
, bitch?
, hound?
, pup!
, dog!
, bitch!
, hound!
, puppy
, latexpup


█ UNDEAFEN KEYWORDS
claps her hands.
claps his hands.
claps their hands.
time's up!
wake up, pet
wstawaj


# ███████ 🅐 COMPREHENSION

Difficulty= 3
ForgetTaughtWords= NO
CompCensorMethod= RANDOM
CompCensorSymbols=  ▓ , ▒ , ░

█ VOCABULARY (auditory)
# Write words below that you want to teach your puppy! (And delete this entire line when you do)
up, down, bow, sit, puppy, puppies, ponies, kennel, leash, ponifia, good, wrong, bad, left, right, back, stay, heel, do, nogy, noga, fetch, stoat, mnie, szybciej, wolniej, chodź, biegaj, biegac, w, levo, prawo, stoj

█ EXCEPTIONS (comprehension)
# Write things you want your puppy to always understand below. (And delete this entire line when you do)
sit
stand
heel
come
down
up
speak
bye-bye
bark
ruff
roll over
beg
paw


█ CompPREFIX
ante, anti, circum, co-, de, dis, em, en, epi, ex, ex-, extra, fore, homo, hyper, il, im, in, ir, infra,
inter, macro, micro, mid, mis, mono, non, omni, para, post, pre, re, semi, sub super, trans, tri, un, uni

█ CompSUFFIX
er, or, ism, ist, ity, ty, ment, ness, ship, sion, tion, en, ise, ize, able, ible, al, esque, ful, ic, ical, ious,
ous, ish, ive, less, y, ed


# ███████ 🅐 BIMBIFY

BimbifyAuditoryLimit= 6
BimbifyCensorMethod= WORDREPLACE
BimbifyCensorSymbols= ..𝘪𝘮𝘢𝘨𝘪𝘯𝘦 𝘪𝘧 𝘺𝘰𝘶 𝘤𝘰𝘶𝘭𝘥 𝘶𝘯𝘥𝘦𝘳𝘴𝘵𝘢𝘯𝘥 𝘸𝘩𝘢𝘵 𝘵𝘩𝘦𝘺'𝘳𝘦 𝘴𝘢𝘺𝘪𝘯𝘨..,..𝑏𝑎𝑟𝑘 𝑏𝑎𝑟𝑘 𝑏𝑎𝑟𝑘~!..,..𝑏𝑎𝑏𝑏𝑙𝑒 𝑏𝑎𝑏𝑏𝑙𝑒 𝑏𝑎𝑏𝑏𝑙𝑒..,♥ ♥ ♥ ♥


█ EXCEPTIONS (bimbify-auditory)
#Write things you want your pup to be understand below!
sit
stand
heel
come
down
up
speak
bye-bye
owner

# ███████ 🅐 WORD REPLACE
# ███████ Replace words heard by the submissive with selected words of your own.

# ❓ [ Aggressive ] ― Look for matches within words, rather than simply replacing exact words. (for example, if the submissive writes “saddest” and one of the word replacements is “sad=happy”, “sad” within “saddest” will be replaced with “happy”, resulting in “happydest”). As shown, this can introduce imperfect results. Be advised! (minimum length of words to “seek” is 3 letters)

# Separate replacements by ; for a random pick between them!

AGGRESSIVE= NO

█ WORD & REPLACEMENT (auditory)
noga=heel
waruj=sit in front
bierz=bark and try to bite
siad=sit and be calm
mistress=owner
Tricky=Mistress Tricky
Vana=Miss Vana
Mara=Miss Mara
Tori=Miss Tori
Tay=This pup
lol=arooo!
boobs=girl parts
butt=tushy
boobies=wobblies
boob=girl part
breast=bouncy
breasts=bouncies
pussy=kitty
cunt=coochie
vagina=flower
tits=squishies
tit=squeak
titties=perkies
nipple=nubby
nipples=nubbies
booty=tushy
butt=tush
cock=private parts
dick=willy
penis=dangly bits
retard=good girl
dumb=good
retards=special
retarded=super special
fuck=fluffy
fucking=boinking
fucked=boinked
damn=drat
damnit=gosh
crap=dookie
gay=happy
bitch=doggy girl
slut=needy girl
whore=girl who likes pets
kisses=💋💋💋
smiles=( ͡❛ ᴗ ͡❛)
giggles=( ᵔ ᴗ ᵔ) ♥
aphasia=the brain-drainer

# ███████ ❖ V E R B A L  R E S T R I C T I O N S
# ██████████████████████████████████
# ███████ 🅥 SPEAK ON COMMAND

TimeLimit= 360
MsgCountLimit= 20
AllowEmotes= YES
EZMode= NO


█ SPEAK KEYWORDS
speak, hound
speak, pup
speak, dog
speak, mutt
yes, pup?
yes, hound?
yes, mutt?
yes, dog?
speak, everyone
speak, puppy
speak, pet
speak, Latexpup
gadaj


█ SILENCE KEYWORDS
shush, mutt
silence, hound
silence, dog
silence, mutt
silence, everyone
bad doggy! timeout!
shush, puppy
silence, puppy
zamknij sie
zamknij ryje


# ███████ 🅥 BIMBIFY
# ███████ Reduce allowed length of spoken words.

BimboLetterLimit= 6
BimboReplaceMethod= REPLACE, OVER-BIMBO, STRICT
BimboCensorMethod= RANDOM


█ BIMBO-REPLACEMENTS
.grrrr!
..woof!
*barks~!*
*pants~*
..uhhh..
..ruff!

█ EXCEPTIONS (bimbify-verbal)
mistress
owner
Mistress
Owner


# Don't forget spaces at the end (if desired)
█ PREFIXES (over-bimbo)
woof
gruff
grrrr
bark
arooo
..
ruff

# Don't forget spaces at the beginning (if desired)
█ SUFFIXES (over-bimbo)
..!
~!
ble
ly
y
ie
ert
oo
rrrrly
rooooo
!!
oooo!
 ruff

█ EXPRESSIONS (bimbify)
/me pants~!
/me attempts to speak, but dumb beasts cannot do so.
/me stumbles all over its words as they attempt to speak. Arooooo!
/me barks hysterically. It appears as if it's attempting to communicate.. it's not going well.
/me howls. Arooooooooo~!
/me whimpers, rubbing its thighs together.
/me barks~!
/me blinks slowly.
/me wags its tail, its entire behind swaying along. Barkbarkbark!
/me tilts its head, looking clueless.


# ███████ 🅥 WORD REPLACE

vAGGRESSIVE= NO

█ WORD & REPLACEMENT (verbal)
tori=Miss Tori
ginger=Miss Tori
gingy=Miss Tori
vana=Miss Vana
tricky=Mistress Tricky
mara=Miss Mara
tay=This pup
taylor=This pup
rahi=Rarrrrr-hie!
ferri=Ferrr-hie!
axelle=Miss arrf-xelle
i=pup
am=is
are=is
i'm=pup is
i'll=pup will
i've=pup has
i'd=pup would
me=puppy
my=puppy's
mine=puppy's
myself=puppy's self
have=has
like=likes
keep=keeps
would=wood
dislike=bad-like
dislikes=bad-likes
hello=aroooooo;barkings;woofday
hi=woof
heya=*pants*
bye=bye-bye! bye-bye!
goodbye=many bye-byes! Arooooo!
lol=Arooooooo!
ha=huff-heh!
people=beings
children=pups
child=pup
boobies=wobblies
boob=bouncy
breast=breasticle
breasts=bouncies
tits=squishies
tit=squeak
titties=perkies
nipple=nubby
nipples=nubbies
butt=jigglies
butts=pillows
cock=floppy
cocks=floppies
penis=donger
penises=dongers
shaft=stick
dumb=good
retard=special
retarded=super special
tail=swishy
tails=swishies
bird=squeak
birds=squeaks
dog=woof
dogs=woofs
#puppy=tiny-woof
puppies=yappers
cat=mew
cats=mews
kitties=mew-mews
kitty=mew-mew
kitten=tiny-meow
kittens=tiny-meows
elf=knife ear
elves=pointy-ears
bunny=hoppity-hopper
bunnies=hoppity-hoppers
teeth=gnashers
fish=blubbers
food=yum-yum
feed=give treats
foods=yum-yums
snack=tasty-tasty treat
snacks=tasty-tasty treats
fuck=boink
fucking=boinking
fucked=boinked
fook=boonk
fooked=boonked
bitch=dog-girl
shit=dookie
heck=tushy
damn=duh!
gay=same-crotch-lover
day=light
morning=early-light
evening=early-dark
night=dark
cum=icky
small=tiny-small
big=big-large
smart=big-head
smarter=more big-head
hair=fur
hairs=furs
wow=bo-wow!
cuck=cuckoo
triggered=very-very-grr
hood=fake-face
mouth=suck-hole
suck=slurp
sucking=slurping
meat=corpse
ball=sphere
corset=tight-tummy
skirt=shortdress
shopping=spend shinies
money=shinies
cash=sparkles
orgasm=good pets
orgasms=many good-pets
chastity=safe-belt
barn=moo-moo house
cell=itty bitty room
cells=itty bitty rooms
aphasia=brain-drainer
hands=paws
feet=hindpaws
nose=sniffer
smiles=( ͡❛ ᴗ ͡❛)
kisses=💋
giggles=( ᵔ ᴗ ᵔ) ♥
<3=♥
no=noh! ~howl~
yes=ruff, yesh!
tuna=smelly kitty food!
pony=clip clop
ponies=clip clops
mares=snorters
mare=snorter
home=kennel


# ███████ 🅥 GARBLE

█ LETTER & REPLACEMENT (verbal)
y=yh
r=w
k=kh
q=kw
d=h
t=h
z=sh
l=w
w=fh
v=fh
x=ks


# ███████ 🅥 DIALECT
# ███████ Assign to a distinct group. Once enabled, the user will be able to communicate fluently only with users of the same group.

GROUP=dog
█ EXPRESSIONS (dialect)
/me barks~!
/me ruffs!
/me howls loudly. Arooooooo!
Woof!
Bark bark!
Woof woof woof!
Arooooo!
Arf Arf
/me pants~.