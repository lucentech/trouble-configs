ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.12"

val freemarkerVersion = "2.3.31"
val yamlVersion = "2.0"

lazy val root = (project in file("."))
  .settings(
    name := "trouble-configs",
    libraryDependencies ++= Seq(
      "org.freemarker" % "freemarker" % freemarkerVersion,
      "org.yaml" % "snakeyaml" % yamlVersion
    )
  )
